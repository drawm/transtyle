/* Node */
import * as fs from 'fs';
import * as promisify from 'util.promisify';
import * as path from "path";
/* Sass/css */
import * as sass from 'node-sass';
import * as autoprefixer from 'autoprefixer';
import * as postcss from 'postcss';
import * as CleanCSS from 'clean-css';

/* Types */
export type Logger = (...args) => void;
export type Step = (progressBar, skinName: string, outDirectory: string, log: Logger) => (Result) => Promise<Result>;
export type Result = {
  css: string,
};

/* Promises */
export const sassRender = promisify(sass.render);
export const writeFile = promisify(fs.writeFile);

export const logger = (verbose: boolean, quiet: boolean) => (...args): void => {
  if (verbose && !quiet) {
    console.log('\n', ...args);
  }
};
export const rejectWithStylePath = (stylePath) => (error) => Promise.reject({error, stylePath});

/* Steps */
export const autoPrefix: Step = (progressBar, stylePath: string, outDirectory: string, log: Logger) => async ({css}: Result) => {
  const file = path.basename(stylePath);
  try {
    const result = await postcss([autoprefixer]).process(css);

    result
      .warnings()
      .forEach((warn) => {
        log(`[${file}] (autoprefixer:WARNING)`, warn.toString());
      });

    progressBar.tick({file, step: 'Auto Prefix'});
    progressBar.render();
    return result;

  } catch (error) {
    log(`[${file}] (autoprefixer:ERROR)`, error);
    throw error;
  }
};

export const minify: Step = (progressBar, stylePath: string, outDirectory: string, log: Logger) => async (result: Result) => {
  const file = path.basename(stylePath);
  progressBar.tick({file, step: 'Minify'});
  progressBar.render();

  try {
    const output = new CleanCSS({}).minify(result.css);

    output
      .warnings
      .forEach((warn) => {
        log(`[${file}] (minify:WARNING)`, warn.toString());
      });

    return {css: output.styles};
  } catch (error) {
    log(`[${file}] (minify:ERROR)`, error);
    throw error;
  }
};

export const writeToDisk: Step = (progressBar, stylePath: string, outDirectory: string, log: Logger) => async (result: Result) => {
  const file = path.basename(stylePath);
  const fileName:string = file.split('.')[0]+'.css';
  log(`[${file}] Sass compiled successfully`);
  progressBar.tick({file, step: 'Write to disk'});
  progressBar.render();

  log(`[${file}] ${outDirectory+fileName}`);

  return writeFile(outDirectory+fileName, result.css)
    .then(rejectIfWriteFailed(progressBar, file, outDirectory, log));
};

export const rejectIfWriteFailed: Step = (progressBar, stylePath: string, outDirectory: string, log: Logger) => async (writeError) => {
  const file = path.basename(stylePath);
  if (writeError) {
    log(`[${file}] Write to disk failed`);
    return rejectWithStylePath(stylePath)(writeError);
  }

  log(`[${file}] Write to disk successful`);

  // No errors during the compilation, write this result on the disk
  return undefined;
};

