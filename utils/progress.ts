import {noop} from 'lodash';
import * as ProgressBar from 'progress';

export const getFactory = (verbose, quiet): GiveProgress => {
  if (quiet) {
    return progressToNothing;
  } else if (verbose) {
    return progressToStdout;
  }

  return progressBar;
};

type GiveProgress = (tickCount: number) => IProgress;

interface IProgress {
  tick: (options: any) => void,
  render: () => void,
  terminate: () => void,
}

const progressToNothing: GiveProgress = (tickCount: number) => ({
  tick: noop,
  render: noop,
  terminate: noop,
});

const progressToStdout: GiveProgress = (tickCount: number) => ({
  tick: (...args) => console.log('tick', ...args),
  render: noop,
  terminate: noop,
});

const progressBar: GiveProgress = (tickCount: number) => new ProgressBar(
  'Compiling skins (:percent) [:bar] (:step :skinName)',
  {
    total: tickCount, // +1 for setup tick
    width: 100,
    renderThrottle: 1,
  }
);
