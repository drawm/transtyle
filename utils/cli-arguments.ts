import * as yargs from 'yargs';
import {isString} from 'util';
import {Argv} from "yargs";

class CliArguments {
  get argv() {
    return this.yargv.argv;
  }

  constructor(private yargv: Argv) {
  }

  out() {
    this.yargv = this.yargv.option('o', {
      alias: 'out',
      default: './out/',
      describe: 'Path to compiled filess',
      type: 'string',
    })
      .example('$0 --out path/to/my/css/','')
      .example('$0 -o path/to/my/css/','')
      .wrap(null)
      .help('h');

    return this;
  };

  path() {
    this.yargv = this.yargv.option('p', {
      alias: 'path',
      default: './',
      describe: 'Path to all filess',
      type: 'string',
    })
      .example('$0 --path path/to/my/files/','')
      .example('$0 -p path/to/my/files/','')
      .wrap(null)
      .help('h');

    return this;
  };

  files() {
    this.yargv = this.yargv.option('s', {
      alias: 'files',
      default: [],
      describe: 'Target filess',
      type: 'array',
    })
      .example('$0 --files volta luzia','')
      .example('$0 -s volta luzia','')
      .wrap(null)
      .help('h');

    return this;
  };

  verbose() {
    this.yargv = this.yargv.option('v', {
      alias: 'verbose',
      default: false,
      describe: 'Print information to stdout',
      type: 'boolean',
    })
      .example('$0 --verbose','')
      .example('$0 -v','')
      .wrap(null)
      .help('h');

    return this;
  };

  quiet() {
    this.yargv = this.yargv.option('q', {
      alias: 'quiet',
      default: false,
      describe: 'Keep output to stdout to a minimum',
      type: 'boolean',
    })
      .example('$0 --quiet','')
      .example('$0 -q','')
      .wrap(null)
      .help('h');

    return this;
  };

  watch() {
    this.yargv = this.yargv.option('w', {
      alias: 'watch',
      default: false,
      describe: 'Recompile when file changes',
      type: 'boolean',
    })
      .example('$0 --watch','')
      .example('$0 -w','')
      .wrap(null)
      .help('h');

    return this;
  };

  bail() {
    this.yargv = this.yargv.option('b', {
      alias: 'bail',
      default: false,
      describe: 'Exit when an error occur',
      type: 'boolean',
    })
      .example('$0 --bail','')
      .example('$0 -b','')
      .wrap(null)
      .help('h');

    return this;
  };

}

const cliArgs = (new CliArguments(yargs))
  .files()
  .verbose()
  .watch()
  .quiet()
  .bail()
  .path()
  .out()
  .argv;

export const files = cliArgs.s.filter(files => isString(files) && !!files);
export const path = cliArgs.p;
export const out = cliArgs.o;
export const verbose = cliArgs.v;
export const watch = cliArgs.w;
export const bail = cliArgs.b;
export const quiet = cliArgs.q;
