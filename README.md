# Sass compile tool

### Install
```bash
cd path/to/repo
npm install
```
### Usage
Compile all scss file in the current folder.
Output all css files to `./out`
```bash
npm run build
```

Options:
```bash
Options:
  --version      Show version number  [boolean]
  -s, --files     Target files  [array] [default: []]
  -v, --verbose  Print information to stdout  [boolean] [default: false]
  -w, --watch    Recompile when file changes  [boolean] [default: false]
  -q, --quiet    Keep output to stdout to a minimum  [boolean] [default: false]
  -b, --bail     Exit when an error occur  [boolean] [default: false]
  -p, --path     Path to all files  [string] [default: "./"]
  -o, --out      Path to compiled files  [string] [default: "./out/"]
  -h             Show help  [boolean]

Examples:
  npm run build -- --files volta luzia
  npm run build -- -s volta luzia
  npm run build -- --verbose
  npm run build -- -v
  npm run build -- --watch
  npm run build -- -w
  npm run build -- --quiet
  npm run build -- -q
  npm run build -- --bail
  npm run build -- -b
  npm run build -- --path path/to/my/files/
  npm run build -- -p path/to/my/files/
  npm run build -- --out path/to/my/css/
  npm run build -- -o path/to/my/css/
```

Examples:
```bash
npm run build -- --watch --path ../someSite/style --out ../someSite/style
```
