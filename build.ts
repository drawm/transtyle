/* Node */
import * as fs from 'fs';
/* Utils */
import {reduce} from 'lodash';
import * as progress from './utils/progress';
import * as chokidar from 'chokidar';
import * as glob from 'glob';
/* Sass/css */
import {
  bail,
  quiet,
  files as skins,
  out,
  path as pathToSkins,
  verbose,
  watch
} from './utils/cli-arguments';
import {
  autoPrefix,
  logger,
  Logger,
  minify,
  rejectWithStylePath,
  Result,
  sassRender,
  Step,
  writeToDisk
} from './utils/steps';
import * as path from 'path';


let isCompiling = false;


const chainNextStep = (progressBar, stylePath: string, outDirectory: string, log: Logger) => (builder, step) => (
  builder.then(step(progressBar, stylePath, outDirectory, log))
);
const stepsToPromises = (steps: Step[], progressBar, stylePath: string, outDirectory: string, log: Logger) => (result: Result) => (
  reduce(
    steps,
    chainNextStep(progressBar, stylePath, outDirectory, log),
    Promise.resolve(result),
  )
);

const compileSkins = async (styles: string[],
                            outDirectory: string,
                            log: Logger,
                            bail: boolean,
                            verbose: boolean,
                            quiet: boolean,
                            steps: Step[],) => {
  if (isCompiling) {
    return;
  }

  log('Ready to compile', {skins: styles});
  log(`Skins : ${styles.join(', ')}`);

  const tickCount = styles.length * (steps.length + 1);// +1 for setup tick

  const progressFactory = progress.getFactory(verbose, quiet);
  const progressBar = progressFactory(tickCount);

  isCompiling = true;
  try {
    const compilingPromises = styles.map(async (stylePath) => {
      const file = path.basename(stylePath);
      const outFile = outDirectory + file;

      progressBar.tick({file, step: 'Setup'});
      progressBar.render();

      const reject = rejectWithStylePath(stylePath);
      const doAllSteps = stepsToPromises(steps, progressBar, stylePath, outDirectory, log);
      log(`[${file}] Compiling file ${stylePath}`);

      return sassRender({file: stylePath, outFile})
        .then(doAllSteps)
        .catch(reject);
    });

    await Promise.all(compilingPromises);

  } catch (error) {
    console.error(JSON.stringify(error));
    if (bail) {
      process.exit(1);
    }
    throw(error);
  } finally {
    progressBar.terminate();
    isCompiling = false;
  }
};

const optionally = (flag, step: Step): Step => (
  flag
    ? step
    : (progressBar, skinName: string, fileDirectory: string, log: Logger) => async (result: Result) => {
      // All steps should tick to properly indicate the remaining % of execution
      progressBar.tick();
      return result;
    }
);

const setup = async (skins: string[], pathToSkins: string, outDirectory: string, verbose: boolean, quiet: boolean, watch: boolean, bail: boolean) => {
  if (!fs.existsSync(outDirectory)) {
    fs.mkdirSync(outDirectory);
  }

  const log = logger(verbose, quiet);
  const steps = [
    autoPrefix,
    optionally(!watch, minify),
    writeToDisk,
  ];

  if (!skins.length) {
    log(`Looking for ${pathToSkins}*.scss`);
    skins = glob.sync(pathToSkins + '*.scss');
  }


  log({skins, verbose, watch, bail});

  const skinPaths = skins.map(skin => path.resolve(pathToSkins + skin));
  try {
    await compileSkins(skinPaths, outDirectory, log, bail, verbose, quiet, steps);
  } catch (error) {
    throw error;
  }

  if (watch) {
    const compileAndWait = () => {
      compileSkins(skinPaths, outDirectory, log, bail, verbose, quiet, steps)
        .then(() => {
          console.log('Waiting for changes...');
        })
        .catch((error) => {
          throw error;
        });
    };

    chokidar
      .watch(path.resolve(__dirname, '../../css'), {ignoreInitial: true})
      .on('all', compileAndWait);
  }

};

setup(skins, pathToSkins, out, verbose, quiet, watch, bail)
  .catch(error => console.error(error));
